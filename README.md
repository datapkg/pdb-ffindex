# pdb-ffindex

[![docs](https://img.shields.io/badge/docs-v0.1-blue.svg)](https://datapkg.gitlab.io/pdb-ffindex/)
[![build status](https://gitlab.com/datapkg/pdb-ffindex/badges/master/build.svg)](https://gitlab.com/datapkg/pdb-ffindex/commits/master/)

PDB archive converted into *.ffindex and *.ffdata files.

## Notebooks
